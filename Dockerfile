# image avec JDK et apk pour les packages
FROM openjdk:8-alpine
WORKDIR /var/
# installation Git
RUN apk add --no-cache git
# récupération code Java
RUN git clone https://gitlab.com/MB_IB/tp_0620_java.git
WORKDIR /var/tp_0620_java/
# compilation
RUN javac Log.java
# Exécution en utilisant le pilote MySQL
ENTRYPOINT java -cp "./mysql-connector-java-8.0.20.jar:./" Log
#ENTRYPOINT ls
